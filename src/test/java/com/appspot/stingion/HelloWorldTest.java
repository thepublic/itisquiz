package com.appspot.stingion;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HelloWorldTest {

    private HelloWorld helloWorld;

    @Before
    public void setUp() throws Exception {
        helloWorld = new HelloWorld();
    }

    @After
    public void tearDown() throws Exception {
        helloWorld = null;
    }

    @Test
    public void testIsHelloWorldPresent() throws Exception {
        assertTrue("Hello World isn't present", helloWorld.isHelloWorldPresent());
    }
}