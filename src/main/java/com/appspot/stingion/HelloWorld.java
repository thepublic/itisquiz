package com.appspot.stingion;

import java.util.stream.Stream;

/**
 * Created by joe on 31.12.15.
 */
public class HelloWorld {
    public boolean isHelloWorldPresent(){
        return Stream.of("Hello, all!", "Hello World!").filter(s -> s.equals("Hello World!")).findFirst().isPresent();
    }
}
